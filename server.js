const express = require("express");
//const users=require("./routes/users");
const bodyparser = require("body-parser");
const mongoose = require("mongoose");
const cors = require('cors');
require("dotenv").config();
const port = process.env.PORT || 3002;

const path = require("path");
const { response } = require("express");

const app = express()

app.use(bodyparser.json())
app.use(express.static('public'))
app.use(bodyparser.urlencoded({
    extended: true
}))
app.use(cors({
    origin: 'http://localhost:4200'
}));
// const corsOpts = {
//     origin: '*',

//     methods: [
//       'GET',
//       'POST',
//     ],

//     allowedHeaders: [
//       'Content-Type',
//     ],
//   };

// app.use(cors(corsOpts));
var index = require('./routes/router');
app.use(index);
app.listen(port)
module.exports.server = this.server