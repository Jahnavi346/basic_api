const express = require('express')
const { MONGO_URI } = process.env;
const jwt = require('jsonwebtoken')
const router = express.Router()
const validator = require('validator')
const bcrypt = require("bcrypt")
const mongoose = require("mongoose")
const ObjectID = require('mongodb').ObjectID;
const User = require("./models/user")
const { body, validationResult } = require('express-validator')
const { remove } = require('./models/user')
const auth = require("../middleware/auth");
mongoose.connect('mongodb://localhost:27017/register');
var db = mongoose.connection;
db.on('error', () => console.log("Error connecting todatabase"));
db.once('open', () => console.log("connected to database"))
router.post("/signup", async (req, res) => {
    var name = req.body.name;
    var email = req.body.email;
    var password = req.body.password;
    var dob = req.body.dob;
    if (!name || !email || !password || !dob) {
        return res.status(403).json("Please enter all fields");
    }
    name = name.trim();
    email = email.trim();
    //var user = await db.collection('persons').find({$or:[{name},{email} ]}).toArray();
    var user = await db.collection('persons').find({ email }).toArray();
    if (user.length) {
        return res.status(403).json('User already existed with given data');
    }
    let userobj = new User({
        name: name,
        email: email,
        password: password,
        dob: dob
    })
    userobj.save().then(userobj => {
        const obj = userobj.toObject();
        delete obj.password;
        return res.status(200).json(obj);
    })
        .catch(error => {
            var errorstr = new String()
            for (var key in userobj) {
                if (error.errors[key] && key != "toString") {
                    errorstr = errorstr + key + " validation Error " + ',';
                }
            }
            return res.status(403).json(errorstr);
        })
    bcrypt.genSalt(10, function (err, Salt) {
        bcrypt.hash(password, Salt, function (err, hash) {
            if (err) {
                return res.status(403).json('user not registered due to password encryption problem');
            }
            userobj.password = hash
            userobj.save()
        });
    });
})
router.post("/login", (req, res) => {
    var email = req.body.email;
    var password = req.body.password;
    if (!email || !password) {
        return res.status(403).json("Please Enter all fields");
    }
    User.findOne({ email: { $eq: email } }).then((user) => {
        if (!user) {
            return res.send("Email not found")
        }
        bcrypt.compare(password, user.password,
            async function (err, isMatch) {
                if (isMatch) {
                    const token = jwt.sign({ user_id: user._id, email }, process.env.TOKEN_KEY,
                        {
                            expiresIn: "12h",
                        }
                    );
                    user.token = token
                    user.save()
                    return res.send(user)

                }
                if (!isMatch) {

                    return res.status(403).json("Wrong password entered")
                }
            })
    })
})
//body('upemail').isEmail().normalizeEmail()
router.patch("/updateUser", auth, async (req, res) => {
    var upemail = req.body.email;
    //var updob = req.body.dob;
    if (!req.query.id) {
        return res.status(403).json("id is mandatory");
    }
    var upid = req.query.id
    if (upemail) {
        var person = await db.collection('persons').find({ $or: [{ email: upemail }] }).toArray();
        if (person.length) {
            return res.status(403).json('User already existed with given data');
        }
    }
    var user = await db.collection('persons').findOne({});
    var c = 0;
    for (var key in user) {
        if (req.body[key]) {
            if (key == "email") {
                db.collection('persons').findOneAndUpdate({ _id: ObjectID(upid) }, { $set: { email: req.body[key] } })
                c++
            }
            if (key == "name") {
                db.collection('persons').findOneAndUpdate({ _id: ObjectID(upid) }, { $set: { name: req.body[key] } })
                c++
            }
            if (key == "dob") {
                db.collection('persons').findOneAndUpdate({ _id: ObjectID(upid) }, { $set: { dob: req.body[key] } })
                c++
            }
        }
        if (!req.body[key]) {
        }
    }
    if (c == 0) {
        return res.status(403).json("enter the data")
    }
    var users = await db.collection('persons').findOne({ _id: new ObjectID(upid) }, { projection: { name: 1, email: 1, dob: 1 } })
    return res.send(users)

})
router.get("/details", auth, async (req, res) => {
    var upid = req.query.id;
    if (!upid) {
        return res.status(403).json("id is mandatory");
    }
    var users = await db.collection('persons').findOne({ _id: new ObjectID(upid) }, { projection: { name: 1, email: 1, dob: 1 } })
    if (!users) {
        return res.status(403).json("no user found")
    }

    var users = await db.collection('persons').findOne({ _id: new ObjectID(upid) }, { projection: { name: 1, email: 1, dob: 1 } })
    return res.send(users)

})
router.delete("/deleteUser", auth, async (req, res) => {
    var upid = req.query.id;
    if (!req.query.id) {
        return res.status(403).json("id is mandatory");
    }
    var user = await db.collection('persons').findOne({ _id: new ObjectID(upid) }, { projection: { name: 1, email: 1, dob: 1 } })
    const users = await db.collection('persons').deleteOne({ _id: new ObjectID(upid) });
    if (!user) {
        return res.status(403).json("user not found")
    }
    if (!users) {
        return res.status(403).json("user not deleted")
    }
    return res.send(user)
    //return res.send(users)
})
router.patch("/forgotPassword", async (req, res) => {
    var email = req.body.email
    var uppassword = req.body.password
    var strongregexp = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    var users = await db.collection('persons').findOne({ email: email })
    if (users) {
        bcrypt.genSalt(10, function (err, Salt) {
            bcrypt.hash(uppassword, Salt, function (err, hash) {
                if (err) {
                    return res.status(403).json('user not registered due to password encryption problem');
                }
                if (!strongregexp.test(uppassword)) {
                    return res.status(403).json("password validation error");
                }
                db.collection('persons').findOneAndUpdate({ email }, { $set: { password: hash } })
                delete users.password
                res.send(users)
            })
        })
    }
    if (!users) {
        return res.status(403).json("Email not found")
    }
})
router.get("/isTokenValid", auth, async (req, res) => {
    const users = await db.collection('persons').findOne({ _id: new ObjectID(req.user.user_id) }, { projection: { name: 1, email: 1, dob: 1 } });
    return res.send(users)
})
router.get("/getUsers", auth, async (req, res) => {
    const users = await db.collection('persons').find({}, { projection: { name: 1, email: 1, dob: 1 } }).toArray();
    if (users.length) {
        return res.send(users);
    }
    else {
        return res.send("data does not exist");
    }
})
module.exports = router