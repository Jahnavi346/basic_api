const mongoose = require('mongoose')
const validator = require('validator')
const Schema = mongoose.Schema
var strongregexp = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
const userSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        validate(value) {
            if (!validator.isEmail(value))
                res.send('Enter correct format of your email')
        }
    },
    password:
    {
        type: String,
        required: true,
        validate(value) {
            if (!strongregexp.test(value)) {
                res.status(403).json("password is wrong");
            }
        }
    },
    dob: {
        type: Date,
        required: true
    },
    token: {
        type: String

    }
}, { timestamps: false })

const user = mongoose.model('persons', userSchema)
module.exports = user